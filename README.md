# Fuse Community Example

## How to Run

### how to run locally
    mvn clean spring-boot:run

### To deploy in Openshift
    mvn clean package -Popenshift fabric8:deploy